# refillcatkeys

Utility for updating `catkeys` files (used by
the [Locale kit](https://www.haiku-os.org/docs/api/group__locale.html)
in [Haiku](https://www.haiku-os.org)).

Propagates changes from automatically generated catkeys file to manually translated ones.

## Example

    refillcatkeys --reference en locales

Will first read `locales/en.catkeys`, then update each other `catkeys` file in
`locales`. The update happens as follows:

1. The language name is preserved, application MIME and checksum are updated
   from the reference `catkeys` file.
   file, and in the same order.
2. Where keys already exist in the file being updated; the existing translation
   is preserved.
3. Where keys are missing from the file being updated; they are added using the
   translation from the reference file.
4. Existing keys/translations which are no longer in the reference file are
   removed.
5. The keys are sorted to the same order as in the reference file.
