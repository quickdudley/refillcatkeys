use std::collections::HashMap;
use std::io::BufRead;
use clap::Parser;

struct Target {
    language: String,
    current: HashMap<(String, String), String>,
}

struct Source {
    mime: String,
    checksum: u128,
    fresh: Vec<(String, String, String)>,
}

#[derive(Parser, Debug)]
#[clap(author, version, about)]
struct Args {
    #[clap(short, long)]
    reference: String,
    directory: std::path::PathBuf,
}

impl Target {
    fn read<R: BufRead>(src: R) -> std::io::Result<Self> {
        let mut lines = src.lines();
        let line = lines.next().ok_or_else(|| {
            std::io::Error::new(std::io::ErrorKind::InvalidData, "Unexpected end of file")
        })??;
        let mut parts = line.split('\t');
        parts.next(); // 1
        let language = parts.next().map(String::from).ok_or_else(|| {
            std::io::Error::new(std::io::ErrorKind::InvalidData, "Missing language field")
        })?;
        let mut current = HashMap::new();
        for line in lines {
            let line = line?;
            if !line.is_empty() {
                let mut parts = line.split('\t');
                let en = parts.next().unwrap_or_else(|| unreachable!());
                let ctx = parts.next().ok_or_else(|| {
                    std::io::Error::new(std::io::ErrorKind::InvalidData, "Missing context field")
                })?;
                parts.next(); // Last field is a double-tab
                let fl = parts.next().ok_or_else(|| {
                    std::io::Error::new(std::io::ErrorKind::InvalidData, "Missing translation")
                })?;
                current.insert((String::from(en), String::from(ctx)), String::from(fl));
            }
        }
        Ok(Self { language, current })
    }

    fn write<W: std::io::Write>(self, mut sink: W, source: &Source) -> std::io::Result<()> {
        write!(sink, "1\t{}\t{}\t{}\n", self.language, source.mime, source.checksum)?;
        for (en, ctx, fallback) in &source.fresh {
            write!(sink, "{}\t{}\t\t{}\n", en, ctx, self.current.get(&(en.clone(), ctx.clone())).unwrap_or(&fallback))?;
        }
        Ok(())
    }
}

impl Source {
    fn read<R: BufRead>(src: R) -> std::io::Result<Self> {
        let mut lines = src.lines();
        let line = lines.next().ok_or_else(|| {
            std::io::Error::new(std::io::ErrorKind::InvalidData, "Unexpected end of file")
        })??;
        let mut parts = line.split('\t');
        parts.next(); // 1
        parts.next(); // Language name
        let mime = parts.next().ok_or_else(|| {
            std::io::Error::new(std::io::ErrorKind::InvalidData, "Missing MIME field")
        })?;
        let checksum = parts.next().ok_or_else(|| {
            std::io::Error::new(std::io::ErrorKind::InvalidData, "Missing checksum field")
        })?;
        let checksum = checksum
            .parse()
            .map_err(|err| std::io::Error::new(std::io::ErrorKind::InvalidData, err))?;
        let mut fresh = Vec::new();
        for line in lines {
            let line = line?;
            if !line.is_empty() {
                let mut parts = line.split('\t');
                let en = parts.next().unwrap_or_else(|| unreachable!());
                let ctx = parts.next().ok_or_else(|| {
                    std::io::Error::new(std::io::ErrorKind::InvalidData, "Missing context field")
                })?;
                parts.next(); // Double-tab
                let fl = parts.next().ok_or_else(|| {
                    std::io::Error::new(std::io::ErrorKind::InvalidData, "Missing translation")
                })?;
                fresh.push((String::from(en), String::from(ctx), String::from(fl)));
            }
        }
        Ok(Self {
            mime: String::from(mime),
            checksum,
            fresh,
        })
    }
}

fn main() -> std::io::Result<()> {
    use std::fs::File;
    let args = Args::parse();
    let mut source = args.directory.clone();
    source.push(format!("{}.catkeys", args.reference));
    let source = Source::read(std::io::BufReader::new(File::open(source)?))?;
    for entry in args.directory.read_dir()? {
        let entry = entry?.path();
        if let Some(filename) = entry.file_name() {
            let filename = filename.to_string_lossy();
            if let Some((code, blank)) = filename.split_once(".catkeys") {
                if blank.is_empty() && code != &args.reference {
                    let target = Target::read(std::io::BufReader::new(File::open(&entry)?))?;
                    target.write(File::create(&entry)?, &source)?;
                    println!("Written: {:?}", entry);
                }
            }
        }
    }
    Ok(())
}
